

var inputColor = document.querySelector("#color");
var lineWidth = document.querySelector("#lineWidth");
var obj = {
    color: "black",
    lineWidth:10,
    moveTo:[],
};
var allArr = [];
var paint = document.querySelector("#canvas1");
var ctx = paint.getContext("2d");
var clear = document.getElementById("clear");
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
var draw = false;
clearCanvas = () => {
    ctx.clearRect(0, 0, paint.width, paint.height);
    allArr = [];
};
clear.addEventListener("click", clearCanvas);
inputColor.addEventListener("change",(e) => {
    ctx.strokeStyle = e.target.value;
});


lineWidth.addEventListener("change", (e)=>{
    ctx.lineWidth = e.target.value;
});


paint.addEventListener("mousedown", (e)=>{
    draw = true;
    ctx.beginPath();
    ctx.moveTo(e.offsetX, e.offsetY);
    obj = {
        moveTo:[e.offsetX, e.offsetY],
        lineTo:[],
    };
});

paint.addEventListener("mousemove", (e)=>{
    if(!draw) return;
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    obj.color = ctx.strokeStyle;
    obj.lineWidth = ctx.lineWidth;
    obj.lineTo.push(e.offsetX, e.offsetY);
});
paint.addEventListener('mouseup', () => {
    draw = false;
    allArr.push(obj);
});
paint.addEventListener('mouseout', () => draw = false);





function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}






