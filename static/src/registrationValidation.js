import {webpackLocalHost} from "./helpers/const";
import {localhostServ} from "./helpers/const";
import {changeLanguage} from "./helpers/language/language.js";
var selectElementLanguage = document.getElementById("selectElementLanguage");
selectElementLanguage.onchange = () => {changeLanguage("reg")};
main();

function main() {
    if (sessionStorage.getItem("language")!=null){
        selectElementLanguage.value=sessionStorage.getItem("language");
        changeLanguage("reg");
    }else{
        changeLanguage("reg");}
}

let registrationBtn = document.getElementById("registrationBtn");
registrationBtn.addEventListener("click", getValidation);
let err = "";
let User = function () {
    this.login = document.getElementById("login").value.replace(/\s/g, '');
    this.password = document.getElementById("password1").value;
    this.password2 = document.getElementById("password2").value;
    this.email = document.getElementById("email").value;
    this.phone = document.getElementById("phone").value.replace(/[+()-/\s]/g, '');
    this.keyword = document.getElementById("keyword").value;

}
console.log(document.getElementById("keyword").value);
function getValidation() {
    let elementValue = new User();
    let message = document.getElementById("message");
    if (checkLogin(elementValue.login) && checkPass(elementValue.password, elementValue.password2) && checkEmail(elementValue.email) && checkPhone(elementValue.phone, "380")) {
        delete elementValue.password2;
        registrationBtn.disabled=true;
        let xhr = new XMLHttpRequest();
        xhr.open("POST", `${localhostServ}/registration`);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(JSON.stringify(elementValue));
        xhr.onreadystatechange = function () {
            if (xhr.status === 400) {
                if (selectElementLanguage.value === "ru") {
                    err = "Такой логин уже существует";
                } else {
                    err = "This login already exists"
                }
                document.getElementById("errorLogin").innerHTML = err;
                document.getElementById("login").style = "border-color: #900;\n" + " background-color: #FDD";
                registrationBtn.disabled=false;
            } else if(xhr.status === 200){
                message.innerHTML = "Регистрация успешна";
                setTimeout(document.location.href = `${webpackLocalHost}/index.html`, 3000);
            }
        };
        }
}

function checkLogin(login) {
    if (login.length < 5 || !isNaN(Number(login[0])) || login.match(/[?!,.а-яА-ЯёЁ\s]+$/i) || login.length > 30) {
       if(selectElementLanguage.value==="ru"){err = "Некорректный логин"}else
       {err="Incorrect login"}
        document.getElementById("errorLogin").innerHTML=err;
        document.getElementById("login").style="border-color: #900;\n" +" background-color: #FDD";
        return false;

    }
    document.getElementById("errorLogin").innerHTML="";
    document.getElementById("login").style="none";
    return true;
}

function checkPass(pass1, pass2) {
    if (pass1.length < 5 || pass1.length > 15) {
        if(selectElementLanguage.value==="ru"){err = "Некорректный пароль"}else
        {err="Incorrect password"}
        document.getElementById("errorPassword1").innerHTML=err;
        document.getElementById("password1").style="border-color: #900;\n" +" background-color: #FDD";
        return false;
    } else if(pass1 !== pass2){
        if(selectElementLanguage.value==="ru"){err = "Пароли не совпадают"}else
        {err="Passwords do not match"}
        document.getElementById("errorPassword2").innerHTML=err;
        document.getElementById("password2").style="border-color: #900;\n" +" background-color: #FDD";
        return false;
    }
    document.getElementById("password1").style="none";
    document.getElementById("password2").style="none";
    document.getElementById("errorPassword1").innerHTML = "";
    document.getElementById("errorPassword2").innerHTML = "";
    return true;
}

function checkPhone(phone, countriCode) {
    if (phone.length !== 12 || phone.substring(0, 3) != countriCode) {
        if(selectElementLanguage.value==="ru"){err = "Некорректрый телефон"}else
        {err="Incorrect phone"}
        document.getElementById("errorPhone").innerHTML=err;
        document.getElementById("phone").style="border-color: #900;\n" +" background-color: #FDD";
        return false;
    }
    document.getElementById("phone").style="none";
    document.getElementById("errorPhone").innerHTML="";
    return true;
}
function checkEmail(email) {
    if (email.includes("@")) {
        let domen = email.substring(email.indexOf("@"), email.length);
        if (domen.length > 2) {
            document.getElementById("errorEmail").innerHTML="";
            document.getElementById("email").style="none";
            return true;
        }
    }
    if(selectElementLanguage.value==="ru"){err = "Некорректный e-mail"}else
    {err="incorrect email"}
    document.getElementById("errorEmail").innerHTML=err;
    document.getElementById("email").style="border-color: #900;\n" +" background-color: #FDD";
    return false;
}
let closeBtn = document.getElementById("close");
closeBtn.addEventListener('click',() => {document.location.href = `${webpackLocalHost}/index.html`});


import "../public/css/registration.less"
