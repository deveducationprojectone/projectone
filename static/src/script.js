import {changeLanguage} from './helpers/language/language';
import {renderGroup, inputEnebled, postRequestGroup} from "./helpers/groupCallbacks";
import * as utils from "./helpers/utils";
import {checkOnlyNumbers, checkValueLength} from "./helpers/scriptValidation";
import "../public/css/index.less";
import {localhostServ} from "./helpers/const";
import {webpackLocalHost} from "./helpers/const";
import calculatorModule from './helpers/calculatorModule'
import '../public/css/helpers/calculatorModule.less'

import convertMoney from "./helpers/convertLength";
import convertLength from "./helpers/convertMoney";
import canvas from "./helpers/canvas"
import logo1 from '../public/logo.png';
import batman from '../public/batman.png';
import fullScreen from '../public/full.svg'
import settings from './images/settings.png';
var createButton = document.getElementById("Create");
var table = document.getElementById("resulttable");
var modes = document.getElementById("modes");
var exitCabinet = document.getElementById('exitCabinet');
var groupWrapper = document.querySelector(".group-wrapper");
var selectElementLanguage = document.getElementById("selectElementLanguage");
//var clearWrapper = document.getElementById("delete-wrapper");
var nameStudent = document.getElementById("Name");
var lastName = document.getElementById("Lastname");
var city = document.getElementById("City");
var ageInput = document.getElementById("Age");
var clearInputBtn = document.getElementById("clearInput");
var fullscreen = document.getElementById("fullscreen");

var hidenSettings = document.getElementById("settingsHidden");
var btnCloseModal = document.getElementById("exitSettings");
var settingsBtn = document.getElementById("modalSetting");

settingsBtn.addEventListener("click", displayHidenSettings);
btnCloseModal.addEventListener("click", displayNoneSettings);

function displayHidenSettings(){
    hidenSettings.classList.toggle("hidden");
}
function displayNoneSettings(){
    hidenSettings.classList.toggle("hidden");
}

ageInput.oninput = checkOnlyNumbers;
nameStudent.oninput = function () {
    checkValueLength(this, 16)
};
lastName.oninput = function () {
    checkValueLength(this, 16)
};
city.oninput = function () {
    checkValueLength(this, 16)
};
// clearWrapper.onclick = function () {
//     var wrap = document.getElementById("wrapper")
//     console.log(wrap)
//     wrap.style.display = "none";
// }
var wrapGroup = document.getElementById("wrap-group");
wrapGroup.addEventListener("dblclick", inputEnebled);
var addGroupBtn = document.getElementById("add-group");
addGroupBtn.addEventListener("click", renderGroup);
//addGroupBtn.addEventListener('click', postRequestGroup);

var wrapGroups = document.getElementById("group-wrapper");
wrapGroups.addEventListener("click", tabClick);

var eventGroupId;

createButton.addEventListener("click", createStudent);

var avatar = document.getElementById("myAccount-wrapper")
avatar.onclick = function(e){
    console.log(e.target)
    //e.target.style.background = "url('../public/settings.png')";
}
exitCabinet.addEventListener('click', function () {
    document.location.href = `${webpackLocalHost}/index.html`
});
selectElementLanguage.onchange = () => {changeLanguage("index")};

var xhr = new XMLHttpRequest();
var id;
var allInputs;
var allInputsCurrent = [];
var allChildOfTable;
var notificationValue = false;
var ok = document.createElement("button");
ok.innerText = "ok";
ok.classList.add("btn");
ok.addEventListener("click", updateInfo);
var cancel = document.createElement("button");
cancel.innerText = "cancel";
cancel.classList.add("btn");

main();

function main() {
    if (sessionStorage.getItem("language")!=null){
        selectElementLanguage.value=sessionStorage.getItem("language");
        changeLanguage("index");
    }else{
        changeLanguage("index");}
}

function toggleBilling(arg) {
    for (var i = 0; i < arg.length; i++) {
        arg[i].disabled = !arg[i].disabled;
    }
}

table.addEventListener("click", function (e) {
    if (e.target.tagName !== 'BUTTON') return;

    if(e.target.innerText === "cancel") {
        togglerDisabledUpdateButtons(allChildOfTable);
        var allInputs = e.target.parentNode.parentNode.querySelectorAll(".row_childs");
        var childCount = 0;
        var buttonCount = 0;
        for (var element of allInputs) {
            element.disabled = true;
            if (childCount === 4) {
                for (var el of element.children) {
                    if (buttonCount < 2) {
                        el.style.display = "inline-block";
                        buttonCount++
                    }else {
                        el.style.display = "none";
                    }
                }
            }
            childCount++;
        }
    }
});

table.addEventListener("click", function (e) {
    if (e.target.tagName !== 'BUTTON') return;
    var rows = document.getElementsByClassName("row");

    id = parseInt(e.target.getAttribute("id"));

    var str = "";

    if(e.target.innerText === "Update"){
        var upd = document.getElementById(e.target.getAttribute("id"));
        var del = document.getElementById(`${id}del`);
        allInputs = e.target.parentNode.parentNode.querySelectorAll(".row_childs");


        allChildOfTable = e.target.parentNode.parentNode.parentNode.querySelectorAll(".row_childs");
        togglerDisabledUpdateButtons(allChildOfTable);
        getInputsValueIntoArray(allInputs, allInputsCurrent);

        del.style.display = "inline-block";
        upd.style.display = "inline-block";

        for (var element of allInputs) {
           element.addEventListener('keypress', (event) => {checkValueLength(event.target, 16)});
            for (var el of element.children) {
                if(el.textContent === "ok" || el.textContent === "cancel") {
                    el.style.display = "inline-block";
                }else {
                    e.target.parentNode.append(ok);
                    e.target.parentNode.append(cancel);
                }
            }
        }
        toggleBilling(allInputs);
        del.style.display = "none";
        upd.style.display = "none";

    } else if(e.target.innerText === "Delete") {
        allInputs = e.target.parentNode.parentNode.querySelectorAll(".row_childs");
        getInputsValueIntoArray(allInputs, allInputsCurrent);
        deleteRow(e.target);
        if (notificationValue || localStorage.getItem('notificationValue') === "true") {
            notificationMassage(allInputsCurrent, 'notification-container__delete');
            allInputsCurrent.length = 0;
        }
    }
});

function updateInfo(e) {
    togglerDisabledUpdateButtons(allChildOfTable);
    var arrValues = [];
    for (var key in allInputs) {
        if (allInputs[key].value !== undefined) {
            arrValues.push(allInputs[key].value)
        }
    }

    var data = {
        id: id,
        username: arrValues[0],
        lastname: arrValues[1],
        age: arrValues[2],
        city: arrValues[3]
    };
    localStorage.setItem("student_id", id);
    xhr.open("POST", `${localhostServ}/update`);

    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(data));
    xhr.onreadystatechange = function () {
        if (xhr.status == 401) {
            alert("insert correct login or password")
        } else if (xhr.readyState === 4) {
            if (notificationValue || localStorage.getItem('notificationValue') === "true") {
                notificationMassage(compareRequestData(allInputsCurrent, arrValues), 'notification-container__update');
                allInputsCurrent.length = 0;
            }
            var studid = localStorage.getItem("student_id");
            toggleBilling(allInputs);
            var childCount = 0;
            var buttonCount = 0;
            // console.log(allInputs[4])
            for (var element of allInputs) {
                if (childCount === 4) {
                    for (var el of element.children) {
                        if (buttonCount < 2) {
                            el.style.display = "inline-block";
                            buttonCount++
                        }else {
                            el.style.display = "none";
                        }
                    }
                }
                childCount++;
            }
        }
    };
    e.stopPropagation();
}

function createStudent(e) {
    var data = createObj();
    if (!data) return;

    xhr.open("POST", `${localhostServ}`);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(data));
    xhr.onreadystatechange = function () {
        if (xhr.status === 200 && xhr.readyState === 4) {

            var userData = {
                username: document.getElementById("Name").value,
                lastname: document.getElementById("Lastname").value,
                age: document.getElementById("Age").value,
                city: document.getElementById("City").value,
                groups_id: eventGroupId,
                user_id: JSON.parse(this.response)[0].user_id
            };
            var divRow = createRow(userData);

            table.insertAdjacentHTML("beforeend", divRow);
        }
    };
    xhr.onerror = function () {
        alert("server error");
    };
    e.stopPropagation();
}

function createObj(id) {
    var nameOfStudent1 = document.getElementById("Name");
    var ageOfStudent1 = document.getElementById("Age");
    var lastNameOfStudent1 = document.getElementById("Lastname");
    var city1 = document.getElementById("City");
    var group = document.getElementById("Group");

    var userData = {
        username: nameOfStudent1.value,
        age: ageOfStudent1.value,
        lastname: lastNameOfStudent1.value,
        city: city1.value,
        groups_id: eventGroupId,
    };
    if (userData.username === "" || userData.lastname === "" || userData.age === "" ||
        userData.city === "" || userData.groups_id === undefined) {
        return false
    } else {
        return userData;
    }
}

function deleteRow(eventButton) {
        utils.getServerResponse({id: id}, "delete", ()=>{
              eventButton.parentNode.parentNode.innerHTML = null;
    });

}

document.addEventListener("DOMContentLoaded", function (e) {

    var div = document.getElementById("indexLoginName");
    div.innerText = localStorage.getItem("loginName");
    var img = document.createElement("img");
    img.setAttribute("src", "bfe5fd63c5124fbb3730c5b9e2d3bc01.png")
    img.setAttribute("id", "batman-icon")
    img.classList.add("logo");
    avatar.addEventListener("click", function () {
        document.location.href = `${webpackLocalHost}/accountSettings.html`
    })
    avatar.append(img);
    if(localStorage.getItem("img") !== null){
        document.getElementById("batman-icon").src = localStorage.getItem("img")
    }
    utils.getServerResponse({teachers_id: localStorage.getItem("teachers_id")}, "getAllGroups", renderGroupTabs);
});

function renderGroupTabs(data) {
    if (!Array.isArray(data)) {
        return;
    }
    for (var tab of data) {
        var allroups = document.getElementsByClassName("group-wrapper__item");
        if (allroups.length >= 4) {
            return false
        }
        var relativeDiv = document.getElementById("group-wrapper");
        var newGroup = document.createElement("div");
        var closeBtn = document.createElement("button");
        closeBtn.innerText = "X";
        closeBtn.classList.add("btn");
        closeBtn.setAttribute("id", `${tab.groups_id}`);
        newGroup.setAttribute("class", "group-wrapper__item");
        var inputGroup = document.createElement("input");
        inputGroup.setAttribute("class", "toggle-students input-item");
        inputGroup.setAttribute("disabled", "true");
        inputGroup.setAttribute("id", `${tab.groups_id}`);
        inputGroup.value = tab.groupname;
        relativeDiv.prepend(newGroup);
        newGroup.append(inputGroup)
        newGroup.append(closeBtn)
    }
}

function groupOkCancel(event, groupId) {
    console.log(groupId)
    event.disabled = true;
    var div = document.createElement("div");
    div.classList.add("notification-groups");

    var p = document.createElement("p");
    p.setAttribute("id", "notification-groups-text");
    p.innerText = "Are you sure you want to delete this group and all the students in it?"
    div.append(p);
    var ok = document.createElement("button");
    ok.classList.add("btn");
    ok.innerText= "ok";
    div.append(ok);
    ok.addEventListener("click", ()=>{
        deleteGroup(groupId);
        div.style.display = "none";
        event.disabled = false;
        document.location.reload()
    });
    var cancel = document.createElement("button")
    cancel.classList.add("btn");
    cancel.innerText = "cancel";
    div.append(cancel);
    cancel.addEventListener("click", ()=>{
        div.style.display = "none"
        event.disabled = false;
    })
    event.after(div)
}

function createRow(studentData) {
    var btnUpdate;
    var btnDelete;
    var input = "";
    var controlUpdateDelete = "";
    for (var key in studentData) {
        const {user_id: userId} = studentData;
        btnUpdate = `<button id = "${userId}upd"  class="btn btnUpdate">Update</button>`;
        btnDelete = `<button id = "${userId}del"  class="btn btnDelete">Delete</button>`;
        controlUpdateDelete = `<div class="row_childs"> ${btnUpdate} ${btnDelete} </div>`;
        if (key !== "user_id" && key !== "groups_id" && key !== "group_name") {
            input += `<input class="row_childs" value="${studentData[key]}" disabled/>`;
        }
    }
    return `<div class='row ' id='row'> ${input} ${controlUpdateDelete}</div>`;
}
var groupTargetToChange;
wrapGroups.addEventListener("dblclick", (event)=>{
    if (event.target.tagName !== "INPUT" || event.target.id == "insertGroup") {
        return false;
    }
    groupTargetToChange  = event.target;
    groupTargetToChange.disabled = false;
    groupTargetToChange.addEventListener("focusout", (e)=>{
        var nameGroup = e.target.value;
        var id = e.target.getAttribute("id");

        updateGroupName(nameGroup, id);

    })

})

function updateGroupName(nameGroup, id) {
    var data = {
        name:nameGroup,
        id: id
    };
    xhr.open("POST", `${localhostServ}/updateGroup`);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.status === 200 && xhr.readyState === 4) {
            console.log(this.response)
            groupTargetToChange.disabled = true;
        }
    };
    xhr.onerror = function () {
        alert("server error");
    };
    xhr.send(JSON.stringify(data));
}



function tabClick(event) {
    if(event.target.tagName === "BUTTON" && !isNaN(+event.target.getAttribute("id"))){
       // event.target.parentNode.innerHTML = null;
        var groupId = event.target.getAttribute("id");
        // console.log(event.target.getAttribute("id"))
        console.log(groupId)
        groupOkCancel(event.target, groupId);

    }

    if (event.target.tagName !== "INPUT" || event.target.id == "insertGroup") {
        return false;
    }

    var target = event.target;
    var tabId = target.getAttribute("id");
    eventGroupId = tabId;
    utils.setActiveStateByTarget(target, groupWrapper);
    utils.getServerResponse({name: tabId}, "groupStudent", redrawTable);
}


function deleteGroup(groupId) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", `${localhostServ}/deleteGroup`);
    var data = {groupId: groupId};

    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(data));

    xhr.onload = function () {
        console.log("ok")
    }
    insertGroup.setAttribute("disabled","true");
    xhr.onerror = function(){
        console.log("server error");
    };
}


function redrawTable(data) {
    if (!Array.isArray(data)) {
        return;
    }

    // console.log(table)
    table.innerHTML = "";
    for (var student of data) {
        var divRow = createRow(student);
        table.insertAdjacentHTML('beforeend', divRow);
    }

}




clearInputBtn.addEventListener("click", ()=>{
    clearInputBtn.style.display = "none"
    var divClearInfo = document.createElement("div")
    divClearInfo.innerText = "Are you sure you want to delete all students?"
    clearInputBtn.after(divClearInfo);
    var okbtn = document.createElement("button");
    okbtn.classList.add("btn");
    okbtn.innerText = "ok";
    divClearInfo.append(okbtn);
    var canselBtn = document.createElement("button");
    canselBtn.classList.add("btn");
    canselBtn.innerText = "cancel";
    divClearInfo.append(canselBtn);
    canselBtn.addEventListener("click", ()=>{
        divClearInfo.style.display = "none";
        okbtn.style.display = "none";
        canselBtn.style.display = "none";
        clearInputBtn.style.display = "block";
    })
    okbtn.addEventListener("click",()=>{
        var xhr = new XMLHttpRequest();
        xhr.open("POST", `${localhostServ}/studentclear`);
        var data = {groupId: eventGroupId};

        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(JSON.stringify(data));

        xhr.onload = function () {
            console.log("ok")
        }
        xhr.onerror = function(){
            console.log("server error");
        };
        document.location.reload()
    })
});


modes.addEventListener("change",function (e) {
    var option = modes.options[modes.selectedIndex].text;
    var wrapper = document.getElementById("wrapper");
    var wrapper1 = document.getElementById("wrapper1");
    var wrapper2 = document.getElementById("wrapper2");
    if(modes.value == "Paint") {
        wrapper.style.display = "none";
        wrapper1.style.display = "flex";
        wrapper2.style.display = "none";
        wrapper3.style.display = "none";
    } else if(modes.value == "Students"){
        wrapper.style.display = "flex";
        wrapper1.style.display = "none";
        wrapper2.style.display = "none";
        wrapper3.style.display = "none";
    } else if(modes.value  == "Converter"){
        wrapper.style.display = "none";
        wrapper1.style.display = "none";
        wrapper2.style.display = "flex";
        wrapper3.style.display = "none";
    }
    else if(modes.value  == "Calculator"){
        wrapper.style.display = "none";
        wrapper1.style.display = "none";
        wrapper2.style.display = "none";
        wrapper3.style.display = "flex";
    }
});
var activeConverterLength= document.getElementById("btnChangeLength");
activeConverterLength.addEventListener("click", ()=>{document.getElementById("wrapperConverterMoney").style.display="none";
    document.getElementById("wrapperConverterLength").style.display="flex"});
var activeConverterMoney= document.getElementById("btnChangeMoney");
activeConverterMoney.addEventListener("click", ()=>{document.getElementById("wrapperConverterLength").style.display="none";
    document.getElementById("wrapperConverterMoney").style.display="flex"});


fullscreen.addEventListener("click", (e)=>{
    console.log(e)
    document.getElementById("direction").requestFullscreen()
});

// notification logic section
function notificationMassage(changesData, styleMassage) {
  for (var i = 0; i < changesData.length; i = i + 2) {
    if (styleMassage === 'notification-container__delete') {
      let updateMassageText = `Student ${changesData[i + 1]} ${changesData[i ]} was delete`;
      setTimeout(() => {notificationDisplay(updateMassageText, `${styleMassage}`)}, 1000);
      return;
    }
    if (changesData[i + 1] === undefined) {
      return;
    }
    let updateMassageText = `${changesData[i]} was change to ${changesData[i + 1]}`;
    setTimeout(() => {notificationDisplay(updateMassageText, `${styleMassage}`)}, 1000);
  }
}

  function compareRequestData(currentData, sendData) {
    var changeData = [];
    for (var i =  0; i < 4; i++) {
      if (currentData[i] !== sendData[i]) {
        changeData.push(currentData[i], sendData[i]);
      }
    }
    return changeData;
}

function notificationDisplay(notificationText, styleMassage) {
  var notificationContainer = document.getElementById('notificationContainer');

  var divMassageNotification = document.createElement('div');
  divMassageNotification.classList.add(styleMassage);
  divMassageNotification.classList.add('notification-container__massage');
  divMassageNotification.innerText = notificationText;
  divMassageNotification.id = 'notificationText';
  notificationContainer.append(divMassageNotification);
  setTimeout(() => {divMassageNotification.remove()}, 3000);
}

function getInputsValueIntoArray(inputsCollection, ArrayOutput) {
  for (var key in inputsCollection) {
    if (inputsCollection[key].value !== undefined) {
      ArrayOutput.push(inputsCollection[key].value)
    }
  }
}
var notificationButton = document.getElementById('notificationButton');
notificationButton.addEventListener('click', notificationToggler);

function notificationToggler() {
    if (notificationValue) {
        notificationValue = false;
        localStorage.setItem("notificationValue", `${notificationValue}`);
    }else {
        notificationValue = true;
        localStorage.setItem("notificationValue", `${notificationValue}`);
    }
}
// end notification

// update disabled logic
function togglerDisabledUpdateButtons(allChildsNode) {
    for (var i = 0; i < allChildsNode.length; i++) {
        if (allChildsNode[i].tagName === "DIV") {
            for (var j = 0; j < 2; j++) {
                allChildsNode[i].children[j].disabled = !allChildsNode[i].children[j].disabled;
            }
        }
    }
}

var reset = document.getElementById("reset");
reset.addEventListener("click", resetSettings);

function resetSettings(event) {

    localStorage.clear();
    resetSettingRequest();
    document.location.reload();
}

function resetSettingRequest() {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", `${localhostServ}/resetSettings`);
    xhr.send();

    xhr.onload = function () {
        console.log("ok")
    }
    xhr.onerror = function () {
        console.log("server error");
    };
}