import {changeLanguage} from './helpers/language/language.js';
import {checkValueLength} from "./helpers/scriptValidation";
import "../public/css/accountSettings.less";
import "../public/css/header.less"
import ballsGame from "./helpers/ballsGame"


// import logoTeacher from './images/batman.png';
import backArrow from './images/back-arrow.svg';
// require('svg-url-loader!./images/back-arrow.svg');
import logout from './images/logout.svg';
// require('svg-url-loader!./images/logout.svg');

import {webpackLocalHost} from "./helpers/const";
import {localhostServ} from "./helpers/const";

var fullscreen = document.getElementById('fullscreen');
var exitCabinet = document.getElementById('exitCabinet');

var teacherIcon = document.getElementById('teacherIcon');

// add custom icon
// function showInputDownloadImage

exitCabinet.addEventListener('click', function () {
    document.location.href = `${webpackLocalHost}/index.html`
});

var login = document.getElementById("login");
login.oninput = function () {
    checkValueLength(this, 14);
} ;
var password = document.getElementById("password1");
password.oninput = function () {
    checkValueLength(this, 25);
} ;
var password2 = document.getElementById("password2");
password2.oninput = function () {
    checkValueLength(this, 25);
} ;
var email = document.getElementById("email");
email.oninput = function () {
    checkValueLength(this, 25);
} ;
var phone = document.getElementById("phone");
phone.oninput = function () {
    checkValueLength(this, 13);
} ;

let aboutMyself = document.getElementById("aboutMyself");
aboutMyself.addEventListener('keydown', checkCurrentLengthText);

let aboutMyselfCounter = 500;

let textAreaCount = document.getElementById("textAreaCount");
let changeInputs = document.getElementById("changeInputs");
let SaveBtn = document.getElementById("SaveBtn");
changeInputs.addEventListener('click', () => {
    changeButtonsDisplay(SaveBtn, changeInputs);
    if(selectElementLanguage.value!="ru"){
        document.getElementById("loginTitle").innerText="New Login";
        document.getElementById("password1Title").innerText="New Password";
        document.getElementById("emailTitle").innerText="New Email";
        document.getElementById("phoneTitle").innerText="New Phone";
    }else{
        document.getElementById("loginTitle").innerText="Новый логин";
        document.getElementById("password1Title").innerText="Новый пароль";
        document.getElementById("emailTitle").innerText="Новая почта";
        document.getElementById("phoneTitle").innerText="Новый телефон";
    }

});
SaveBtn.addEventListener("click", () => {changeButtonsDisplay(SaveBtn, changeInputs); getValidation();
    if(selectElementLanguage.value!="ru"){
        document.getElementById("loginTitle").innerText="Login";
        document.getElementById("password1Title").innerText="Password";
        document.getElementById("emailTitle").innerText="Email";
        document.getElementById("phoneTitle").innerText="Phone";
    }else{
        document.getElementById("loginTitle").innerText="Логин";
        document.getElementById("password1Title").innerText="Пароль";
        document.getElementById("emailTitle").innerText="Почта";
        document.getElementById("phoneTitle").innerText="Телефон";
    }} );


let teacher = getUserData();
setTimeout(fillInput, 800);



let closeBtn = document.getElementById("close");
closeBtn.addEventListener('click',() => {document.location.href = `${webpackLocalHost}/table.html`});

var selectElementLanguage=document.getElementById("selectElementLanguage");
selectElementLanguage.onchange = ()=>{changeLanguage("accSet")};

main();
function main(){
    if (sessionStorage.getItem("language")!=null){
        selectElementLanguage.value=sessionStorage.getItem("language");
        changeLanguage("accSet");
    }else{
        changeLanguage("accSet");}
}

function getUserData() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", `${localhostServ}/accountSetting`);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send();
    xhr.onload = function () {
        teacher = JSON.parse(this.response);
        console.log(teacher);
    };
}

function fillInput() {
    document.getElementById("login").value = teacher[0].login;
    document.getElementById("email").value = teacher[0].email;
    document.getElementById("password1").value = teacher[0].password;
    document.getElementById("password2").value = teacher[0].password;
    document.getElementById("phone").value = teacher[0].phone_number;
    document.getElementById("aboutMyself").value = teacher[0].about_myself;
    // document.getElementById("teacherIcon").src = teacher[0].teacher_icon;
}

let err = "";

function getValidation() {
    // let elementValue = new User();
    let elementValue = {
        login: login.value.replace(/\s/g, ''),
    password:password.value,
        password2:   password2.value,
        email:   email.value,
        phone:  phone.value.replace(/[+()-/\s]/g, ''),
        aboutMyself:  aboutMyself = document.getElementById("aboutMyself").value,
        teachers_id: teacher[0].teachers_id,
        // this.icon =  document.getElementById("icon").value;
    };
    console.log(elementValue)
    let message = document.getElementById("message");
    if (checkEmail(elementValue.email) && checklogin(elementValue.login) && checkPass(elementValue.password, elementValue.password2) && checkPhone(elementValue.phone, "380")) {
        let xhr = new XMLHttpRequest();
        xhr.open("POST", `${localhostServ}/accountupdate`);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(JSON.stringify(elementValue));
        message.innerHTML = "Changes included";
        console.log(elementValue);
        // setTimeout(document.location.href = `${webpackLocalHost}/table.html`, 1500);
    } else {
        message.innerHTML = err;
    }
}

function checklogin(login) {
    if (login.length < 1 || !isNaN(Number(login[0]))) {
        err = "Некорректный логин";
        return false;
    }
    return true;
}

function checkPass(pass1, pass2) {
    if (pass1.length < 5 || pass1 !== pass2) {
        err = "Некорректный пароль";
        return false;
    }
    return true;
}

function checkPhone(phone, countriCode) {
    if (phone.length !== 12 || phone.substring(0, 3) !== countriCode) {
        err = "Некорректныйтелефон";
        return false;
    }
    return true;
}
function checkEmail(email) {
    if (email.includes("@")) {
        let domen = email.substring(email.indexOf("@"), email.length);
        if (domen.length > 2) {
            return true;
        }

    }
    err = "Некорректный e-mail";
    return false;
}

function checkCurrentLengthText() {
    if (aboutMyself.value.length > aboutMyselfCounter) {
        return;
    }
   textAreaCount.innerText =  String(aboutMyselfCounter - aboutMyself.value.length);
}

//  toggle buttons change and save
    function inputTogglerDisabled(className) {  // 'custom-input'
        let inputCollection = document.getElementsByClassName( className);
        for (let item of inputCollection) {
            item.disabled === true ? item.disabled = false: item.disabled = true;
        }
}
    function changeButtonsDisplay(button1, button2) {
        if (button2.style.display === 'none') {
            button1.style.display = 'none';
            button2.style.display = 'block';
        }else {
            button1.style.display = 'block';
            button2.style.display = 'none';
        }
        inputTogglerDisabled('custom-input');
    }

//fullscreen logic
fullscreen.addEventListener("click", (e)=>{

    document.getElementById("direction").requestFullscreen();
});


var fileInput = document.getElementById("file-input");

fileInput.addEventListener("change", (e) =>{

    console.log( e.target.files[0])
    var fileReader = new FileReader;
    fileReader.readAsDataURL ( e.target.files[0] );
    fileReader.onload = function ( event ) {
        var data = {
            img : event.target.result
        }
        var datasend = JSON.stringify(data)
        var xhr = new XMLHttpRequest();
        xhr.open("POST",`${localhostServ}/sendImage`);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(datasend);
        xhr.onreadystatechange = function () {
            if(xhr.readyState === 4 && xhr.status === 200){
                var img = document.getElementById("teacherIcon");
                localStorage.setItem("img", `${JSON.parse(this.response)}`)
                img.src = `${JSON.parse(this.response)}`
            }
        }
    }

});

document.addEventListener("DOMContentLoaded", function (e) {
    if(localStorage.getItem("img") !== null){
        document.getElementById("teacherIcon").src = localStorage.getItem("img")
    }
})












// console.log(file)
//   var file = fileInput.files[0]
//     if(window.FormData !== undefined){
//         var data = new FormData();
//         data.append("file", file);
//
//         var xhr = new XMLHttpRequest();
//         xhr.open("POST",`${localhostServ}/sendImage`);
//         // xhr.setRequestHeader("Content-type", "application/json");
//         // xhr.send(JSON.stringify(data));
//         xhr.send(data)
//         xhr.onreadystatechange = function () {
//             if(xhr.readyState === 4 && xhr.status === 200){
//
//                 console.log(this.response)
//
//                 var img = document.getElementById("teacherIcon");
//                 img.src = `${this.response}`
//
//             }
//         }
//         xhr.onerror = function () {
//             console.log("error")
//         }
//     }