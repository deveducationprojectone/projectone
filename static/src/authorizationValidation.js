import {changeLanguage} from './helpers/language/language.js'
import {webpackLocalHost} from "./helpers/const";
import {localhostServ} from "./helpers/const";
import  "../public/css/autorization.less";

var loginAvt = document.getElementById("loginAvt");
var passwordAvt = document.getElementById("passwordAvt");
var logInBtn = document.getElementById("logInBtn");
var registrationBtn = document.getElementById("registrationBtnAvt");
var massage = document.getElementById("message");
var selectElementLanguage=document.getElementById("selectElementLanguage");

var hidenPasword = document.getElementById("wraperForgottenPassword");
var keyWordAvt = document.getElementById("kayWordAvt");
var forgottenLoginAvt = document.getElementById("forgottenLoginAvt");
var getPasswordBtn = document.getElementById("getPassword");
var forgottenPasswordMessage = document.getElementById("forgottenPasswordMessage");
var btnCloseModal = document.getElementById("close");
var forgottenPasswordBtn = document.getElementById("forgottenPassword");

selectElementLanguage.onchange = ()=>{changeLanguage("author")};
loginAvt.onkeydown = function (e) {
    if(!e.key.match(/[0-9\/A-Z]/i)){
        return false
    }
};
main();
function main(){
    if (sessionStorage.getItem("language")!=null){
        selectElementLanguage.value=sessionStorage.getItem("language");
        changeLanguage("author");
    }else{
        changeLanguage("author");}
}
var loginValue = {};
logInBtn.addEventListener("click", getElementValue);
registrationBtn.addEventListener("click", function () {
    document.location.href = `${webpackLocalHost}/registration.html`
});

function getElementValue() {
    loginValue = {login: loginAvt.value, password: passwordAvt.value};
    localStorage.setItem('loginName', loginAvt.value);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", `${localhostServ}/authorization`);
    xhr.setRequestHeader("Content-type", "application/json");

    xhr.onreadystatechange = function () {
        if (xhr.status == 401) {
            document.getElementById("loginAvt").style="border-color: #900;\n" +" background-color: #FDD";
            document.getElementById("passwordAvt").style="border-color: #900;\n" +" background-color: #FDD";
            if (selectElementLanguage.value === "ru") {
               var err = "Введите корректный логин и пароль!";
            } else {
               var err = "Insert correct login or password!!!"
            }
            massage.innerText = err;
        } else if(xhr.status === 200 && xhr.readyState === 4) {
            var newStudentValue = JSON.parse(this.response);
             localStorage.setItem("teachers_id", newStudentValue[0].teachers_id);
            document.location.href = `${webpackLocalHost}/table.html`
        }
    }
    xhr.send(JSON.stringify(loginValue));
}

forgottenPasswordBtn.addEventListener("click", displayHiden);
btnCloseModal.addEventListener("click", displayNone);
var forgottenpassword = {};
getPasswordBtn.addEventListener("click", getPasswordValue);

function getPasswordValue() {
    var passwordValue = {login: forgottenLoginAvt.value, keyword: keyWordAvt.value};
    var xhr = new XMLHttpRequest();
    xhr.open("POST", `${localhostServ}/for`);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(passwordValue));
    xhr.onload = function () {
        if (xhr.status == 401) {
            document.getElementById("forgottenLoginAvt").style="border-color: #900;\n" +" background-color: #FDD";
            document.getElementById("kayWordAvt").style="border-color: #900;\n" +" background-color: #FDD";
            if (selectElementLanguage.value === "ru") {
                forgottenPasswordMessage.value ="Введите корректный логин и слово!";
            } else {
                forgottenPasswordMessage.value = "Insert correct login or kay word!";
            }
        } else {
            forgottenPasswordMessage.value = JSON.parse(this.response)
        }
    }
}

function displayHiden(){
    hidenPasword.classList.toggle("hidden");
}
function displayNone(){
    hidenPasword.classList.toggle("hidden");
}




